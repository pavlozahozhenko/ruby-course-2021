require 'sinatra'

get '/hello' do
  'Hello from Sinatra!'
end

get '/hello/world' do
  'gggg'
end

get '/hello/:username' do |username|
  puts "params: #{params}"
  erb :username, locals: {name: username, surname: params['surname']}
end

post '/hello' do
  'Hello POST method'
end

put /\/smth(\d+)/ do |num|
  puts "params: #{params}"
  "put method #{num}"
end

delete '/anything' do
end
