require 'rack'

class MinApp

  def call(env)
    puts "ENV: #{env}"
    if env['REQUEST_METHOD'] == 'GET' && env['REQUEST_PATH'] == '/hello'
      [
        200,
        {'Content-Type' => 'text/html'},
        ['<h1>Hello, world!</h1>']
      ]
    else
      [
        404,
        {},
        ['Page not found']
      ]
    end
  end
end

Rack::Handler.default.run MinApp.new
