Rails.application.routes.draw do
  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
  devise_for :users
  root to: 'home#index'
  get '/hello', to: 'home#hello'
  get '/hello/student/:id', to: 'home#hello'
  get '/hello/:username', to: 'home#hello'
  resources :users
  resources :posts
end
