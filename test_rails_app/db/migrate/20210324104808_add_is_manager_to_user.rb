class AddIsManagerToUser < ActiveRecord::Migration[6.1]
  def change
    add_column :users, :is_manager, :boolean, null: false, default: false
  end
end
