class ChangeUserEmail < ActiveRecord::Migration[6.1]
  def up
    change_column :users, :email, :string, null: false, default: nil, limit: 200
  end

  def down
    change_column :users, :email, :string, null: false, default: '', limit: 250
  end
end
