class CreateUsers < ActiveRecord::Migration[6.1]
  def change
    create_table :users do |t|
      t.string :first_name, null: false, default: '', limit: 100
      t.string :last_name, null: false, default: '', limit: 100
      t.string :email, null: false, default: '', limit: 250
      t.timestamps
    end
  end
end
