class CreateStudents < ActiveRecord::Migration[6.1]
  def change
    create_table :students do |t|
      t.string :first_name, limit: 100, null: false, default: ''
      t.string :last_name, limit: 100, null: false, default: ''
      t.timestamps
    end
  end
end
