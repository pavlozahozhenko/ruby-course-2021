Feature: Main flow

  Background:
    Given user exists
    And I am on login page
    When I fill in "user[email]" field with "test@example.com"
    And I fill in "user[password]" field with "password"
    And I click on "Sign in"

  @javascript
  Scenario: User logs into the system
    Then I should see text "Hello, students!"

  @javascript
  Scenario: Manager logs into the system and edits an existing user
    Given user is a manager
    And the user exists with email "to_edit@example.com"
    When I go to users
    And I click on "users.list.actions.edit" within user "to_edit@example.com" block
    When I fill in "user[first_name]" field with "Vasya"
    And I click on "Update User"
    Then there should exist a user with the name "Vasya"
