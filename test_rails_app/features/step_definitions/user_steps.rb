Given /^user exists$/ do
  @user = ::FactoryBot.create :user, email: 'test@example.com'
end

Given /^manager exists$/ do
  @user = ::FactoryBot.create :user, email: 'test@example.com', is_manager: true
end

Given /^user is logged in$/ do
  login_as(@user)
end

Given /^user is a manager$/ do
  @user.update_column(:is_manager, true)
end

Given /the user exists with email "(.*)"/ do |email|
  ::FactoryBot.create(:user, email: email)
end

Then /there should exist a user with the name "(.*)"/ do |first_name|
  user = ::User.find_by(first_name: first_name)
  expect(user).not_to be_nil
end
