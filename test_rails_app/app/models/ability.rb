# frozen_string_literal: true

class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= ::User.new
    if user.is_manager?
      can :manage, ::User
    else
      can :read, ::User
    end
  end
end
