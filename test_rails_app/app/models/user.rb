class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable, :trackable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable
  validates :email, :first_name, :last_name, presence: true
  validates :email, format: { with: /\w+@[\w.]+/ }

  has_many :posts
  has_one :settings
  has_many :followings
  has_many :followers, through: :followings

  before_create :do_smth
  after_create do |record|
    puts 'after create triggered'
    puts 'sending welcome email....'
    puts 'sent'
    #TODO: send welcome email
  end

  def full_name
    "#{first_name} #{last_name}"
  end

  private
  
  def do_smth
    puts 'before create triggered'
    self.first_name = first_name.capitalize
    self.last_name = last_name.capitalize
  end
end
