class UsersController < ApplicationController
  before_action :authenticate_user!, except: :index
  authorize_resource except: :index

  def index
    @users = ::User.all
    respond_to do |format|
      format.json do
        render json: @users
      end
      format.html { }
    end
  end

  def new
    @user = ::User.new
  end

  def create
    @user = ::User.create(user_params)
    if @user.valid?
      redirect_to users_path
    else
      render :new
    end
  end

  def edit
    @user = ::User.find params[:id]
    do_some_stuff
  end

  def update
    @user = ::User.find(params[:id])
    @user.update(user_params)
    if @user.valid?
      redirect_to users_path
    else
      render :edit
    end
  end

  def destroy
    @user = ::User.find(params[:id])
    @user.destroy!
    redirect_to users_path
  end

  private

  def do_some_stuff
    UserEditWorker.perform_async(@user.id)
  end

  def user_params
    params.require(:user).permit(:first_name, :last_name, :email)
  end
end
