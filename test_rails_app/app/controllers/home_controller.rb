class HomeController < ApplicationController
  def index
  end

  def hello
    @username = if params[:id].present?
                  ::Student.find(params[:id]).first_name
                else
                  params[:username]
                end
  end
end
