class UserEditWorker
  include Sidekiq::Worker

  def perform(user_id)
    @user = ::User.find user_id
    logger.info "Running user edit worker for #{@user.email}..."
    sleep 10
  end
end
