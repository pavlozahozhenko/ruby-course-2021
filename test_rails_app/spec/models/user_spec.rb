require 'rails_helper'

RSpec.describe User, type: :model do
  context '#full_name' do
    subject { ::User.new(first_name: 'John', last_name: 'Smith') }

    it 'should return first_name + last_name' do
      expect(subject.full_name).to eq('John Smith')
    end

    it 'should return correct last_name if it changes' do
      subject.last_name = 'Snow'
      expect(subject.full_name).to eq('John Snow')
    end
  end
end
