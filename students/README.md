# Project Requirements & Info

### Teams
* Form up the team. It can be either a one-person team (for introvers and/or nerds out there) or it can consist of up to 3 or even 4 people (the experiment continues)
* Come up with a cool name for you team

### Getting started guide
* Clone the repository - `git clone git@gitlab.com:pavlozahozhenko/ruby-course-2021.git`
* Create a branch `example_team_project` and add it to the repository, e.g.:
```
git checkout -b example_team_project
mkdir students/example_team
touch students/example_team/.gitkeep
git add students/example_team/
git commit --message="Added project folder"
git push origin example_team_project
```
* Switch to that branch and work on your project in the corresponding folder (students/example_team)
* When the project is ready - make a pull request to the 'master' branch and I'll do a code review

### Project requirements

#### Mandatory
* Any Rack framework: Ruby on Rails, Sinatra, Hanami, Roda, Cuba, Padrino, etc. Non-rails framework projects instantly get +10 points.
* Authentication.
* Authorization. At least 2 user roles with different access rights.
* At least some client-side interaction should be done via JavaScript (AJAX). Or add a full-fledged SPA to the client-side.
* At least one background process/task (via Sidekiq or its equivalents).
* Automated testing, the bigger coverage the better. At the very least: some unit-tests and an acceptance test covering the main flow.

#### Optional
* Internalization with 2+ locales, no hardcoded texts.
* Bi-directional client-server communication (via ActionCable).
* Getting the app ready for production environment, using a "proper" RDBMS (e.g. PostgreSQL, MySQL etc) and production-grade web servers (e.g. Puma, Unicorn, Thin, Passenger, Falcon etc).

### Trivia
* I can give you some advice on project architecture, useful gems etc.
* In case of project ideas difficulties - I can try to help.
