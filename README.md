# Ruby Course 2021

NaUKMA Ruby on Rails programming course 2021

### Announcements
* Extended project submission deadline to 21.04.2021 10:00
* Switching on the camera will be required during the exam
* There's still some time remaining, go-go-go!

### Course Materials
* [Lecture videos](https://drive.google.com/drive/folders/1Lqi5NqmQHDCwFbrQweC17R3T4BqZ1PDO?usp=sharing)
* [Telegram channel](http://t.me/naukma_ruby_2021)
* [Repository 2020 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2020)
* [Repository 2019 (Gitlab)](https://gitlab.com/pavlozahozhenko/ruby-course-2019)
* [Repository 2017 (BitBucket)](https://bitbucket.org/burius/ruby_course_2017)
* [Repository 2016 (BitBucket)](https://bitbucket.org/burius/ror_course)

### The Project
* [Project requirements](https://gitlab.com/pavlozahozhenko/ruby-course-2021/-/tree/master/students#project-requirements-info)
* Project submission deadline (last commit): 21.04.2021 10:00 UTC+3

### Useful Links
#### Ruby/Rails
* [Official Ruby documentation (core API)](http://ruby-doc.org/core-3.0.0/)
* [Ruby style guide](https://rubystyle.guide/) by bbatsov
* [Rails API documentation](http://api.rubyonrails.org/)
* [Official Rails Guides](https://guides.rubyonrails.org/)
