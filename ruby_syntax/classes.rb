module Util
  def self.util_method
    puts "foo bar"
  end
end

module Growth
  def grow
    @mass ||= 0
    @mass += growth_coeff
  end

  def growth_coeff
    0
  end
end

module Nature
  class Virus
    include Growth
    attr_accessor :r0, :type

    def initialize(r0 = 0)
      puts 'virus init'
      @r0 = r0
      @@total_amount ||= 0
      @@total_amount += 1
    end
  end

  class CoronaVirus < Virus
    GROWTH_COEFF = 0.00003

    def initialize(r0 = 1)
      super(r0)
      puts 'coronavirus init'
    end

    def print_info
      puts "Virus with R0 #{@r0}. Total # of viruses: #{@@total_amount}"
    end

    def total_amount
      @@total_amount
    end

    def total_amount=(value)
      @@total_amount = value
    end

    def growth_coeff
      GROWTH_COEFF
    end

    private

    def private_method
      puts 'private'
    end
  end

  class Covid19 < CoronaVirus
    def total_amount
      puts 'overwritten total amount'
      super
    end
  end
end
    
class Nature::Covid19::DnaNode
  def initialize
    puts 'DNA node init...'
  end
end

module Computer
  class Virus
  end
end

class Animal
  include Growth
end

class Human < Animal
  GROWTH_COEFF = 100_000_000_000

  def growth_coeff
    GROWTH_COEFF
  end
end
