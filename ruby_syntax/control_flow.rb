def foo(b)
  val = case b
        when 1
          b
        when 2
          b * 2
        when 3
          b * 10
        else
          nil
        end
  val * 2
end
