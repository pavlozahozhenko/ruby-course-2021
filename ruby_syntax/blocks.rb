def my_method(param1, param2)
  puts "method called #{param1} #{param2}"
  result = param1 * param2
  result = yield(result) if block_given?
  puts 'other things happen...'
  result * 3
end

def proc_method(proc_array)
  puts 'foo bar'
  result = 0
  proc_array.each do |proc|
    result += proc.call(3)
  end
  result
end
