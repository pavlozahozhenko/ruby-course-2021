class M
  def method_missing(method_name, *params, &block)
    if method_name =~ /^foo_\d+$/
      puts "Method #{method_name} called"
    else
      super
    end
  end

  %w[method1 method2 hello].each do |name|
    define_method name do |a|
      puts "Defined method with param #{a}"
    end
  end
end
